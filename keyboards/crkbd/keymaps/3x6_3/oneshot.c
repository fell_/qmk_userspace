#include "action.h"
#include "custom_keycodes.h"
#include "oneshot.h"
#include "keycode.h"
#include "stdint.h"

static uint8_t mod_up_queued_state = 0;

void update_oneshot(oneshot_state *state, uint16_t mod, uint16_t trigger, uint16_t keycode, keyrecord_t *record) {
    if (keycode == trigger) {
        if (record->event.pressed) {
            // Keydown
            switch (*state) {
                case os_up_unqueued:
                    register_code(mod);
                    break;
                case os_up_queued:
                    mod_up_queued_state |= MOD_BIT(mod);
                    break;
                default:
                    break;
            }

            *state = os_down_unused;
        } else {
            if ((MOD_BIT(mod) & mod_up_queued_state) > 0) {
                unregister_code(mod);
                *state = os_up_unqueued;
                mod_up_queued_state &= ~MOD_BIT(mod);
                return;
            }

            // Keyup
            switch (*state) {
                case os_down_unused:
                    *state = os_up_queued;
                    break;
                case os_down_used:
                    unregister_code(mod);
                    *state = os_up_unqueued;
                    break;
                default:
                    break;
            }
        }
        return;
    }

    if (record->event.pressed) {
        if (is_oneshot_cancel_key(keycode) && *state != os_up_unqueued) {
            *state = os_up_unqueued;
            unregister_code(mod);
        }
    } else {
        if (is_oneshot_ignored_key(keycode)) {
            return;
        }

        // On non-ignored keyup, consider the oneshot used.
        switch (*state) {
            case os_down_unused:
                *state = os_down_used;
                break;
            case os_up_queued:
                *state = os_up_unqueued;
                unregister_code(mod);
                break;
            default:
                break;
        }
    }
}

bool is_oneshot_cancel_key(uint16_t keycode) {
    switch (keycode) {
        case KC_BSPC:
            return true;
        default:
            return false;
    }
}

bool is_oneshot_ignored_key(uint16_t keycode) {
    switch (keycode) {
        case SYM:
        case NAV:
        case NUM:
        case OS_SHFT:
        case OS_CTRL:
        case OS_ALT:
        case OS_GUI:
            return true;
        default:
            return false;
    }
}
