#pragma once

#include "quantum_keycodes.h"

#define SYM OSL(1)
#define NUM OSL(2)
#define NAV OSL(3)

enum custom_keycodes {
    OS_SHFT = SAFE_RANGE,
    OS_CTRL,
    OS_ALT,
    OS_GUI,
};
