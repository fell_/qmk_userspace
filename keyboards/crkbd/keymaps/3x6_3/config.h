#pragma once

#define MASTER_LEFT
#define PERMISSIVE_HOLD

#define TAPPING_TERM 220
#define TAPPING_TOGGLE 2
#define QUICK_TAP_TERM 100

#define ONESHOT_TAP_TOGGLE 2
#define ONESHOT_TIMEOUT 3000

#define COMBO_TERM 30

#define NO_ALT_REPEAT_KEY

#define AUTO_SHIFT_TIMEOUT 140
#define AUTO_SHIFT_REPEAT
// needs to overwrite `autoshift_press_user` to work with callum mods
#define NO_AUTO_SHIFT_MODIFIERS
#define NO_AUTO_SHIFT_NUMERIC
#define NO_AUTO_SHIFT_TAB
