#include QMK_KEYBOARD_H
#include "action.h"
#include "custom_keycodes.h"
#include "keymap_german.h"
#include "lib.h"
#include "matrix.h"
#include "oled.h"
#include "oled_driver.h"
#include "oneshot.h"
// #include "os_detection.h"
#include "process_key_override.h"
#include "tap_hold.h"
#include "quantum_keycodes.h"
#include "g/keymap_combo.h" // IWYU pragma: keep

// OS_UNSURE would be a more appropriate default value. Waking up from USB sleep
// resets this value and prevents re-evaluation of OS detection resulting in
// undesired behaviour.
// os_variant_t OS = OS_LINUX;

const key_override_t **key_overrides = (const key_override_t *[]){&ko_make_basic(MOD_BIT_LALT, DE_S, DE_SS), NULL};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    // clang-format off
    [0] = LAYOUT_split_3x6_3(
        KC_TAB,  DE_Q, DE_W, DE_E, DE_R, DE_T, /*|  |*/ DE_Z, DE_U, DE_I,    DE_O,   DE_P,    DE_UDIA,
        OS_SHFT, DE_A, DE_S, DE_D, DE_F, DE_G, /*|  |*/ DE_H, DE_J, DE_K,    DE_L,   DE_ODIA, DE_ADIA,
        OS_GUI,  DE_Y, DE_X, DE_C, DE_V, DE_B, /*|  |*/ DE_N, DE_M, DE_COMM, DE_DOT, DE_MINS, NAV,
        // ------------------------------------/*|  |*/-------------------------------------------- //
                         OS_CTRL, KC_SPC, SYM, /*|  |*/ NUM, KC_BSPC, OS_ALT
    ),

    [1] = LAYOUT_split_3x6_3(
        KC_TRNS, DE_LABK, DE_LBRC, DE_LCBR, DE_LPRN, DE_TILD, /*|  |*/ DE_PIPE, DE_RPRN, DE_RCBR, DE_RBRC, DE_RABK, KC_NO,
        KC_TRNS, DE_SECT, DE_0,    DE_PERC, DE_BSLS, KC_NO,   /*|  |*/ KC_NO,   DE_SLSH, DE_AMPR, DE_DLR,  DE_EQL,  KC_NO,
        KC_TRNS, KC_NO,   DE_AT,   DE_ACUT, DE_HASH, DE_EXLM, /*|  |*/ DE_QUES, DE_ASTR, DE_GRV,  DE_CIRC, DE_PLUS, KC_NO,
        // ---------------------------------------------------/*|  |*/------------------------------------------------- //
                                     KC_TRNS, KC_TRNS, TG(1), /*|  |*/ KC_TRNS, KC_TRNS, KC_TRNS
    ),

    [2] = LAYOUT_split_3x6_3(
        KC_TRNS, KC_MPLY, KC_MPRV, KC_MNXT, KC_NO, KC_NO, /*|  |*/ KC_F11, KC_F12, KC_VOLD, KC_VOLU, KC_MUTE, KC_NO,
        KC_TRNS, DE_1,    DE_2,    DE_3,    DE_4,  DE_5,  /*|  |*/ KC_F1,  KC_F2,  KC_F3,   KC_F4,   KC_F5,   KC_NO,
        KC_TRNS, DE_6,    DE_7,    DE_8,    DE_9,  DE_0,  /*|  |*/ KC_F6,  KC_F7,  KC_F8,   KC_F9,   KC_F10,  KC_NO,
        // -----------------------------------------------/*|  |*/----------------------------------------------- //
                               KC_TRNS, KC_TRNS, KC_TRNS, /*|  |*/ TG(2), KC_TRNS, KC_TRNS
    ),

    [3] = LAYOUT_split_3x6_3(
        KC_TRNS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, /*|  |*/ KC_NO,   KC_NO,   KC_NO, KC_NO,    KC_NO, KC_NO,
        KC_CAPS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, /*|  |*/ KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT, KC_NO, KC_NO,
        KC_TRNS, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, /*|  |*/ KC_NO,   KC_NO,   KC_NO, KC_NO,    KC_NO, TG(3),
        // -----------------------------------------/*|  |*/---------------------------------------------- //
                         KC_TRNS, KC_TRNS, KC_TRNS, /*|  |*/ KC_TRNS, KC_TRNS, KC_TRNS
    ),
    // clang-format on
};

#ifdef OLED_ENABLE

bool oled_task_user() {
    if (is_keyboard_master()) {
        oled_set_cursor(0, 0);
        oled_layer();
        oled_mods();
        oled_caps();
    } else {
        oled_logo();
    }

    return false;
}

#endif

void keyboard_pre_init_user() {
    setPinOutput(24);
    writePinHigh(24);
}

// bool process_detected_host_os_user(os_variant_t detected_os) {
//     OS = detected_os;
//     return true;
// }

oneshot_state os_shft_state = os_up_unqueued;
oneshot_state os_ctrl_state = os_up_unqueued;
oneshot_state os_alt_state  = os_up_unqueued;
oneshot_state os_gui_state  = os_up_unqueued;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    bool cancel_propagation = (is_oneshot_cancel_key(keycode) && get_mods() > 0) || !process_tap_hold(keycode, record);

    update_oneshot(&os_shft_state, KC_LSFT, OS_SHFT, keycode, record);
    update_oneshot(&os_ctrl_state, KC_LCTL, OS_CTRL, keycode, record);
    update_oneshot(&os_alt_state, KC_LALT, OS_ALT, keycode, record);
    update_oneshot(&os_gui_state, KC_LGUI, OS_GUI, keycode, record);

    if (cancel_propagation) {
        return false;
    }

    switch (keycode) {
        case KC_ESC:
        case KC_CAPS:
            // if (OS != OS_LINUX) return true;
            send_key(keycode == KC_ESC ? KC_CAPS : KC_ESC, record);
            return false;
    }

    return true;
}

void matrix_scan_user() {
    tap_hold_matrix_scan();
}
