#pragma once

#include "action.h"

void send_key(uint16_t keycode, keyrecord_t *record);
bool is_mod_set(uint8_t mods, uint8_t mod_bit);
