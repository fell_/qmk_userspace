#include QMK_KEYBOARD_H
#include "action.h"
#include "custom_keycodes.h"
#include "keymap_german.h"
#include "lib.h"
#include "matrix.h"
#include "oled.h"
#include "oled_driver.h"
#include "oneshot.h"
// #include "os_detection.h"
#include "tap_hold.h"
#include "quantum_keycodes.h"
#include "g/keymap_combo.h" // IWYU pragma: keep

// OS_UNSURE would be a more appropriate default value. Waking up from USB sleep
// resets this value and prevents re-evaluation of OS detection resulting in
// undesired behaviour.
// os_variant_t OS = OS_LINUX;

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    // clang-format off
    // qwertz
    [_QWZ] = LAYOUT_split_3x6_3(
        KC_NO, DE_Q, DE_W, DE_E, DE_R, DE_T, /*|  |*/ DE_Z, DE_U, DE_I,    DE_O,   DE_P,    KC_NO,
        KC_NO, DE_A, DE_S, DE_D, DE_F, DE_G, /*|  |*/ DE_H, DE_J, DE_K,    DE_L,   NAV,     KC_NO,
        KC_NO, DE_Y, DE_X, DE_C, DE_V, DE_B, /*|  |*/ DE_N, DE_M, DE_COMM, DE_DOT, DE_MINS, KC_NO,
        // ----------------------------------/*|  |*/------------------------------------------ //
        // FIX:                                    + USE THIS THUMB KEY +
                      DF(_ALT), KC_SPC, SYM, /*|  |*/ NUM, KC_NO, KC_NO
    ),
    // graphite
    [_ALT] = LAYOUT_split_3x6_3(
        KC_NO, DE_B, DE_L, DE_D, DE_W, DE_Z, /*|  |*/ NAV,  DE_F, DE_O,    DE_U,   DE_J,    KC_NO,
        KC_NO, DE_N, DE_R, DE_T, DE_S, DE_G, /*|  |*/ DE_Y, DE_H, DE_A,    DE_E,   DE_I,    KC_NO,
        KC_NO, DE_Q, DE_X, DE_M, DE_C, DE_V, /*|  |*/ DE_K, DE_P, DE_COMM, DE_DOT, DE_MINS, KC_NO,
        // ----------------------------------/*|  |*/------------------------------------------ //
                      DF(_QWZ), KC_SPC, SYM, /*|  |*/ NUM, KC_NO, KC_NO
    ),

    [_SYM] = LAYOUT_split_3x6_3(
        KC_TRNS, DE_ACUT, DE_LBRC, DE_LCBR, DE_LPRN, DE_LABK, /*|  |*/ DE_RABK, DE_RPRN, DE_RCBR, DE_RBRC, DE_GRV,  KC_NO,
        KC_TRNS, DE_SECT, DE_0,    DE_PERC, DE_BSLS, DE_TILD, /*|  |*/ DE_PIPE, DE_SLSH, DE_AMPR, DE_DLR,  DE_EURO, KC_NO,
        KC_TRNS, DE_EQL,  DE_AT,   KC_NO,   DE_HASH, DE_EXLM, /*|  |*/ DE_QUES, DE_ASTR, KC_NO,   DE_CIRC, DE_PLUS, KC_NO,
        // ---------------------------------------------------/*|  |*/------------------------------------------------- //
                                  KC_TRNS, KC_TRNS, TG(_SYM), /*|  |*/ KC_TRNS, KC_TRNS, KC_TRNS
    ),

    [_NUM] = LAYOUT_split_3x6_3(
        KC_TRNS, KC_MPLY, KC_MPRV, KC_MNXT, KC_NO, KC_NO, /*|  |*/ KC_F11, KC_F12, KC_VOLD, KC_VOLU, KC_MUTE, KC_NO,
        KC_TRNS, DE_1,    DE_2,    DE_3,    DE_4,  DE_5,  /*|  |*/ KC_F1,  KC_F2,  KC_F3,   KC_F4,   KC_F5,   KC_NO,
        KC_TRNS, DE_6,    DE_7,    DE_8,    DE_9,  DE_0,  /*|  |*/ KC_F6,  KC_F7,  KC_F8,   KC_F9,   KC_F10,  KC_NO,
        // -----------------------------------------------/*|  |*/----------------------------------------------- //
                               KC_TRNS, KC_TRNS, KC_TRNS, /*|  |*/ TG(_NUM), KC_TRNS, KC_TRNS
    ),

    // TODO: make modifiers easier to access (move alt/gui/ctrl to base layer as tap-dance)
    [_NAV] = LAYOUT_split_3x6_3(
        KC_TRNS, DE_SS,   KC_NO,   KC_NO,   KC_NO,   KC_NO, /*|  |*/ KC_NO,   KC_NO,   KC_NO, KC_NO,    KC_NO,   KC_NO,
        KC_TRNS, OS_GUI,  OS_CTRL, OS_ALT,  OS_SHFT, KC_NO, /*|  |*/ KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT, TG(NAV), KC_NO,
        KC_TRNS, DE_ADIA, DE_ODIA, DE_UDIA, KC_CAPS, KC_NO, /*|  |*/ KC_NO,   KC_NO,   KC_NO, KC_NO,    KC_NO,   KC_NO,
        // -------------------------------------------------/*|  |*/---------------------------------------------- //
                                 KC_TRNS, KC_TRNS, KC_TRNS, /*|  |*/ KC_TRNS, KC_TRNS, KC_TRNS
    ),
    // clang-format on
};

#ifdef OLED_ENABLE

bool oled_task_user() {
    if (is_keyboard_master()) {
        oled_set_cursor(0, 0);
        oled_layer();
        oled_mods();
        oled_caps();
    } else {
        oled_logo();
    }

    return false;
}

#endif

void keyboard_pre_init_user() {
    setPinOutput(24);
    writePinHigh(24);
}

// bool process_detected_host_os_user(os_variant_t detected_os) {
//     OS = detected_os;
//     return true;
// }

// FIX: handle NAVdn - NAVup - MODdn - KEYdn - KEYup -- MOD should remain active
oneshot_state os_shft_state = os_up_unqueued;
oneshot_state os_ctrl_state = os_up_unqueued;
oneshot_state os_alt_state  = os_up_unqueued;
oneshot_state os_gui_state  = os_up_unqueued;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    bool cancel_propagation = (is_oneshot_cancel_key(keycode) && get_mods() > 0) || !process_tap_hold(keycode, record);

    update_oneshot(&os_shft_state, KC_LSFT, OS_SHFT, keycode, record);
    update_oneshot(&os_ctrl_state, KC_LCTL, OS_CTRL, keycode, record);
    update_oneshot(&os_alt_state, KC_LALT, OS_ALT, keycode, record);
    update_oneshot(&os_gui_state, KC_LGUI, OS_GUI, keycode, record);

    if (cancel_propagation) {
        return false;
    }

    switch (keycode) {
        case KC_ESC:
        case KC_CAPS:
            // if (OS != OS_LINUX) return true;
            send_key(keycode == KC_ESC ? KC_CAPS : KC_ESC, record);
            return false;
    }

    return true;
}

void matrix_scan_user() {
    tap_hold_matrix_scan();
}

bool caps_word_press_user(uint16_t keycode) {
    switch (keycode) {
        // Keys to shift
        case DE_A ... DE_Y: // DE_Y maps to KC_Z -> highest value
        case DE_MINS:
            add_weak_mods(MOD_BIT(KC_LSFT));
            return true;

        // Keys not to shift
        case DE_1 ... DE_0:
        case KC_BSPC:
            return true;

        default:
            return false;
    }
}
