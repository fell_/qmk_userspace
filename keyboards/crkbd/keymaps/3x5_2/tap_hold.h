#include "stdint.h"
#include "action.h"
#include "stdint.h"

void tap_hold(void);
void tap_hold_end(void);
void tap_hold_tap_n(uint16_t keycode, uint8_t n);
void tap_hold_send_hold(uint16_t keycode);

void tap_hold_matrix_scan(void);
bool process_tap_hold(uint16_t keycode, const keyrecord_t *record);
