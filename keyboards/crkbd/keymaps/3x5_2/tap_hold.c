#include "action.h"
#include "keymap_german.h"
#include "quantum.h"
#include "send_string.h"
#include "stdint.h"
#include "timer.h"

static bool     in_progress = false;
static uint16_t timer       = 0;
static uint16_t lastkey     = KC_NO;

bool tap_hold(uint16_t keycode) {
    switch (keycode) {
        case DE_LABK:
        case DE_RABK:
        case DE_AMPR:
        case DE_EQL:
        case DE_PIPE:
        case DE_EXLM:
            return true;
        default:
            return false;
    }
}

void tap_hold_end(void) {
    if (in_progress) {
        in_progress = false;
        tap_code16(lastkey);
    }
}

void tap_hold_tap_n(uint16_t keycode, uint8_t n) {
    for (uint8_t i = 0; i < n; i++) {
        tap_code16(keycode);
    }
}

void tap_hold_send_hold(uint16_t keycode) {
    switch (keycode) {
        case DE_LABK:
        case DE_RABK:
            tap_hold_tap_n(keycode, 2);
            return;
        case DE_AMPR:
        case DE_EQL:
        case DE_PIPE:
            tap_code16(KC_SPC);
            tap_hold_tap_n(keycode, 2);
            tap_code16(KC_SPC);
            return;
        case DE_EXLM:
            tap_code(KC_SPC);
            tap_code16(DE_EXLM);
            tap_code16(DE_EQL);
            tap_code16(KC_SPC);
            return;
        default:
            tap_code16(keycode);
    }
}

void tap_hold_matrix_scan(void) {
    if (in_progress && timer_elapsed(timer) >= AUTO_SHIFT_TIMEOUT) {
        in_progress = false;
        tap_hold_send_hold(lastkey);
    }
}

bool process_tap_hold(uint16_t keycode, const keyrecord_t *record) {
    // Only process tap hold for desired keys
    if (!tap_hold(keycode)) return true;

    if (record->event.pressed) {
        tap_hold_end();
        in_progress = true;
        timer       = timer_read();
        lastkey     = keycode;
    } else {
        if (in_progress && keycode == lastkey && timer_elapsed(timer) < AUTO_SHIFT_TIMEOUT) {
            tap_hold_end();
        }
    }

    return false;
}
