#pragma once

#include "quantum_keycodes.h"

#define _QWZ 0
#define _ALT 1
#define _SYM 5
#define _NUM 6
#define _NAV 7

#define SYM OSL(_SYM)
#define NUM OSL(_NUM)
#define NAV OSL(_NAV)

enum custom_keycodes {
    OS_SHFT = SAFE_RANGE,
    OS_CTRL,
    OS_ALT,
    OS_GUI,
};
