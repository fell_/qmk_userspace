#include QMK_KEYBOARD_H
#include "custom_keycodes.h"
#include "keymap_german.h"
#include "quantum_keycodes.h"
#include "g/keymap_combo.h" // IWYU pragma: keep

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    // clang-format off
    [_DEF] = LAYOUT(
                 KC_CAPS, DE_1, DE_2,    DE_3,   DE_4, DE_5, DE_6,
                 KC_TAB,  DE_Q, DE_W,    DE_E,   DE_R, DE_T, DE_7,
        DE_M,    DE_H,    DE_A, DE_S,    DE_D,   DE_F, DE_G, DE_8,
        DE_N,    KC_LSFT, DE_Y, DE_X,    DE_C,   DE_V, DE_B, DE_9,
        KC_LGUI, KC_LCTL, ALT,  KC_LALT, KC_SPC,             DE_I
    ),

    [_ALT] = LAYOUT(
                 KC_ENT, KC_F1,   KC_F2,   KC_F3,    KC_F4,   KC_F5,   KC_F6,
                 KC_NO,  KC_NO,   KC_UP,   KC_MPRV,  KC_MPLY, KC_MNXT, KC_F6,
        DE_PLUS, KC_NO,  KC_LEFT, KC_DOWN, KC_RIGHT, KC_NO,   KC_NO,   KC_F8,
        DE_MINS, KC_NO,  KC_NO,   KC_NO,   KC_NO,    KC_NO,   KC_NO,   KC_F9,
        QK_BOOT, KC_NO,  KC_NO,   KC_NO,   KC_NO,                      KC_NO
    ),
    // clang-format off
};
